<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\Pdfgenerator\Controller\Adminhtml\Templates;

use Magento\Cms\Controller\Adminhtml\Page\PostDataProcessor;

class PdfDataProcessor extends PostDataProcessor
{

    /**
     * @param array $data
     * @return array
     */
    //@codingStandardsIgnoreLine
    public function validateRequireEntry(array $data)
    {

        $requiredFields = [
            'template_name' => __('Template Name'),
            'template_description' => __('Template description'),
            'store_id' => __('Store View'),
            'template_file_name' => __('Template File Name'),
            'template_paper_ori' => __('Template Paper Orientation'),
            'template_paper_form' => __('Template Paper Form'),
            'is_active' => __('Status')
        ];

        foreach ($data as $field => $value) {
            if (in_array($field, array_keys($requiredFields)) && $value == '') {
                $this->messageManager->addErrorMessage(
                    __('To apply changes you should fill in hidden required "%1" field', $requiredFields[$field])
                );
            }
        }

        return $data;
    }
}
