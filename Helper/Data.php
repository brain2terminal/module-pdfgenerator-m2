<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\Pdfgenerator\Helper;

use B2T\Pdfgenerator\Model\ResourceModel\Pdfgenerator\Collection;
use B2T\Pdfgenerator\Model\ResourceModel\Pdfgenerator\CollectionFactory as templateCollectionFactory;
use B2T\Pdfgenerator\Model\Source\AbstractSource;
use B2T\Pdfgenerator\Model\Source\TemplateActive;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order\Invoice;
use Magento\Store\Model\ScopeInterface;

/**
 * Handles the config and other settings
 *
 * Class Data
 * @package B2T\Pdfgenerator\Helper
 */
class Data extends AbstractHelper
{

    const ENABLE = 'b2t_pdfgenerator/general/enabled';
    const EMAIL = 'b2t_pdfgenerator/general/email';

    /**
     * @var ScopeConfigInterface
     */
    public $config;

    /**
     * @var Collection
     */
    public $templateCollection;

    /**
     * Data constructor.
     * @param Context $context
     * @param templateCollectionFactory $_templateCollection
     */
    public function __construct(
        Context $context,
        templateCollectionFactory $_templateCollection
    ) {
        $this->templateCollection = $_templateCollection;
        $this->config = $context->getScopeConfig();
        parent::__construct($context);
    }

    /**
     * Check if module will send email on new invoice or invoice update
     *
     * @return boolean
     */
    public function isEmail()
    {
        if ($this->isEnable()) {
            return $this->hasConfig(self::EMAIL);
        }
        return false;
    }

    /**
     * Check if module is enable
     *
     * @return boolean
     */
    public function isEnable()
    {
        //@codingStandardsIgnoreLine
        if (!$this->mPDFExists() || !$this->collection()->count()) {
            return false;
        }

        return $this->hasConfig(self::ENABLE);
    }

    /**
     * @return bool
     */
    private function mPDFExists()
    {
        if (class_exists('mPDF')) {
            return true;
        }
        return false;
    }

    /**
     * @param string $configPath
     * @return bool
     */
    public function hasConfig($configPath)
    {
        return $this->config->getValue(
            $configPath,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get the active template
     *
     * @param $invoice
     * @return \Magento\Framework\DataObject
     */
    public function getTemplateStatus(Invoice $invoice)
    {
        $invoiceStore = $invoice->getOrder()->getStoreId();
        $collection = $this->collection();
        $collection->addStoreFilter($invoiceStore);
        $collection->addFieldToFilter('is_active', TemplateActive::STATUS_ENABLED);
        $collection->addFieldToFilter('template_default', AbstractSource::IS_DEFAULT);

        return $collection->getLastItem();
    }
    
    public function getShipmentTemplateStatus(Shipment $shipment)
    {
        
        $shipmentStore = $shipment->getOrder()->getStoreId();
        $collection = $this->collection();
        $collection->addStoreFilter($shipmentStore);
        $collection->addFieldToFilter('is_active', TemplateActive::STATUS_ENABLED);
        $collection->addFieldToFilter('template_default', AbstractSource::IS_DEFAULT);

        return $collection->getLastItem();
    }
    
    public function getOrdertemplateStatus(Order $order)
    {
        $orderStore = $order->getStoreId();
        $collection = $this->collection();
        $collection->addStoreFilter($orderStore);
        $collection->addFieldToFilter('is_active', TemplateActive::STATUS_ENABLED);
        $collection->addFieldToFilter('template_default', AbstractSource::IS_DEFAULT);

        return $collection->getLastItem();
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $collection = $this->templateCollection->create();
        return $collection;
    }
}
