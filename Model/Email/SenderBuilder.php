<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\Pdfgenerator\Model\Email;

use B2T\Pdfgenerator\Model\Pdfgenerator;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use B2T\Pdfgenerator\Helper\Pdf;
use B2T\Pdfgenerator\Helper\Data;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Shipment;

class SenderBuilder extends \Magento\Sales\Model\Order\Email\SenderBuilder
{

    /**
     * @var Pdf
     */
    private $helper;

    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * SenderBuilder constructor.
     * @param Template $templateContainer
     * @param IdentityInterface $identityContainer
     * @param TransportBuilder $transportBuilder
     * @param Pdf $helper
     * @param Data $dataHelper
     * @param DateTime $dateTime
     */
    public function __construct(
        Template $templateContainer,
        IdentityInterface $identityContainer,
        TransportBuilder $transportBuilder,
        Pdf $helper,
        Data $dataHelper,
        DateTime $dateTime
    ) {
        $this->helper = $helper;
        $this->dataHelper = $dataHelper;
        $this->dateTime = $dateTime;
        parent::__construct($templateContainer, $identityContainer, $transportBuilder);
    }

    /**
     * Add attachment to the main mail
     */
    public function send()
    {
        $vars = $this->templateContainer->getTemplateVars();
        $this->_checkInvoice($vars);
        $this->_checkOrder($vars);
        $this->_checkShipment($vars);
        
        parent::send();
    }

    /**
     * Add attachment to the css/bcc mail
     */
    public function sendCopyTo()
    {
        $vars = $this->templateContainer->getTemplateVars();
        $this->_checkInvoice($vars);
        parent::sendCopyTo();
    }

    /**
     *
     * Check if we need to send the invoice email
     *
     * @param $vars
     * @return $this
     */
    private function _checkInvoice($vars)
    {
        if (!$this->dataHelper->isEmail()) {
            return $this;
        }

        if ($vars['invoice'] instanceof Invoice) {
            $invoice = $vars['invoice'];
            $helper = $this->helper;

            $helper->setInvoice($invoice);

            /** @var Pdfgenerator $template */
            $template = $this->dataHelper->getTemplateStatus($invoice);

            if (empty($template->getId())) {
                return $this;
            }

            $helper->setTemplate($template);

            $pdfFileData = $helper->template2Pdf();

            $date = $this->dateTime->date('Y-m-d_H-i-s');

            $this->transportBuilder->addAttachment(
                $pdfFileData['filestream'],
                \Zend_Mime::TYPE_OCTETSTREAM,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $pdfFileData['filename'] . $date . '.pdf'
            );
        }

        return $this;
    }
    
        /**
     *
     * Check if we need to send the order email
     *
     * @param $vars
     * @return $this
     */
    private function _checkOrder($vars)
    {
        if (!$this->dataHelper->isEmail()) {
            return $this;
        }

        if ($vars['order'] instanceof Invoice) {
            $order = $vars['order'];
            $helper = $this->helper;

            $helper->setInvoice($order);

            /** @var Pdfgenerator $template */
            $template = $this->dataHelper->getOrderTemplateStatus($order);

            if (empty($template->getId())) {
                return $this;
            }

            $helper->setTemplate($template);

            $pdfFileData = $helper->template2Pdf();

            $date = $this->dateTime->date('Y-m-d_H-i-s');

            $this->transportBuilder->addAttachment(
                $pdfFileData['filestream'],
                \Zend_Mime::TYPE_OCTETSTREAM,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $pdfFileData['filename'] . $date . '.pdf'
            );
        }

        return $this;
    }
        /**
     *
     * Check if we need to send the shipping email
     *
     * @param $vars
     * @return $this
     */
    private function _checkShipment($vars)
    {
        if (!$this->dataHelper->isEmail()) {
            return $this;
        }

        if ($vars['shipment'] instanceof Shipment) {
            $shipment = $vars['shipment'];
            $helper = $this->helper;

            $helper->setInvoice($shipment);

            /** @var Pdfgenerator $template */
            $template = $this->dataHelper->getShipmentTemplateStatus($shipment);

            if (empty($template->getId())) {
                return $this;
            }

            $helper->setTemplate($template);

            $pdfFileData = $helper->template2Pdf();

            $date = $this->dateTime->date('Y-m-d_H-i-s');

            $this->transportBuilder->addAttachment(
                $pdfFileData['filestream'],
                \Zend_Mime::TYPE_OCTETSTREAM,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                $pdfFileData['filename'] . $date . '.pdf'
            );
        }

        return $this;
    }
}
