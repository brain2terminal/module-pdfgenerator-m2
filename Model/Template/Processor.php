<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\Pdfgenerator\Model\Template;

use Magento\Email\Model\Template;
use Magento\Framework\App\Area;
use Magento\Framework\DataObject;

/**
 * Class Processor
 * @package B2T\Pdfgenerator\Model\Template
 */
class Processor extends Template
{

    /**
     * @var store id;
     */
    private $storeId;

    /**
     * Configuration of design package for template
     *
     * @var DataObject
     */
    private $designConfig;

    /**
     * @return mixed
     * get the pdf template body
     */
    public function getTemplateBody()
    {
        return $this->getTemplate()->getTemplateBody();
    }

    /**
     * @return mixed
     */
    public function getTemplateHeader()
    {
        return $this->getTemplate()->getTemplateHeader();
    }

    /**
     * @return mixed
     */
    public function getTemplateFooter()
    {
        return $this->getTemplate()->getTemplateFooter();
    }

    /**
     * @return mixed
     */
    public function getTemplateFileName()
    {
        return $this->getTemplate()->getTemplateFileName();
    }

    /**
     * Get processed template
     *
     * @return string
     * @throws \Magento\Framework\Exception\MailException
     */
    public function processTemplate()
    {
        // Support theme fallback for email templates
        $isDesignApplied = $this->applyDesignConfig();

        $processor = $this->getTemplateFilter()
            ->setUseSessionInUrl(false)
            ->setPlainTemplateMode($this->isPlain())
            ->setIsChildTemplate($this->isChildTemplate())
            ->setTemplateProcessor([$this, 'getTemplateContent']);

        $processor->setVariables($this->getVariables());
        $this->setUseAbsoluteLinks(true);
        $html = $this->html($processor);

        if ($isDesignApplied) {
            $this->cancelDesignConfig();
        }

        return $html;
    }

    /**
     * @param $processor
     * @param $area
     * @return mixed
     */
    private function processArea($processor, $area)
    {
        $textProcessor = $processor
            ->setStoreId($this->storeId)
            ->setDesignParams([0])
            ->filter(__($area));

        return $textProcessor;
    }

    /**
     * @param $processor
     * @return array
     */
    private function html($processor)
    {
        $html = [
            'body' => $this->processArea($processor, $this->getTemplateBody()),
            'header' => $this->processArea($processor, $this->getTemplateHeader()),
            'footer' => $this->processArea($processor, $this->getTemplateFooter()),
            'filename' => $this->processArea($processor, $this->getTemplateFileName()),
        ];

        return $html;
    }

    /**
     * Get design configuration data
     *
     * @return DataObject
     */
    public function getDesignConfig()
    {
        $templates = $this->getTemplate()->getData('store_id');
        $this->storeId = $templates[0];

        if ($this->designConfig === null) {
            //@codingStandardsIgnoreLine
            $this->designConfig = new DataObject(
                ['area' => Area::AREA_FRONTEND, 'store' => $this->storeId]
            );
        }

        return $this->designConfig;
    }
}
