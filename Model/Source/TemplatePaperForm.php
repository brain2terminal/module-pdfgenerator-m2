<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\Pdfgenerator\Model\Source;

use Magento\Framework\View\Model\PageLayout\Config\BuilderInterface;

/**
 * Class PageLayout
 */
class TemplatePaperForm extends AbstractSource
{
    /**
     * @var \Magento\Framework\View\Model\PageLayout\Config\BuilderInterface
     */
    private $pageLayoutBuilder;

    /**
     * Constructor
     *
     * @param BuilderInterface $pageLayoutBuilder
     */
    public function __construct(BuilderInterface $pageLayoutBuilder)
    {
        $this->pageLayoutBuilder = $pageLayoutBuilder;
    }

    /**
     * Paper types
     */
    const TEMAPLATE_PAPER_FORM_A4 = 1;
    const TEMAPLATE_PAPER_FORMAT_A3 = 2;
    const TEMAPLATE_PAPER_FORMAT_A5 = 3;
    const TEMAPLATE_PAPER_FORMAT_A6 = 4;
    const TEMAPLATE_PAPER_FORMAT_LETTER = 5;
    const TEMAPLATE_PAPER_FORMAT_LEGAL = 6;

    /**
     * Prepare post's statuses.
     *
     * @return array
     */
    public function getAvailable()
    {
        return 
        [
            self::TEMAPLATE_PAPER_FORM_A4 => __('A4'),
            self::TEMAPLATE_PAPER_FORMAT_A3 => __('A3'),
            self::TEMAPLATE_PAPER_FORMAT_A5 => __('A5'),
            self::TEMAPLATE_PAPER_FORMAT_A6 => __('A6'),
            self::TEMAPLATE_PAPER_FORMAT_LETTER => __('Letter'),
            self::TEMAPLATE_PAPER_FORMAT_LEGAL => __('Legal'),
        ];
    }
}
