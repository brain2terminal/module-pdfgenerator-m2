<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\Pdfgenerator\Model;

use \B2T\Pdfgenerator\Api\Data\TemplatesInterface;
use \B2T\Pdfgenerator\Model\ResourceModel\Pdfgenerator as TemplateResource;
use \B2T\Pdfgenerator\Api\TemplatesRepositoryInterface;
use Exception;
use Magento\Framework\Message\ManagerInterface;

class PdfgeneratorRepository implements TemplatesRepositoryInterface
{

    /**
     * @var array
     */
    private $instances = [];

    /**
     * @var TemplateResource
     */
    private $resource;

    /**
     * @var TemplatesInterface
     */
    private $templatesInterface;

    /**
     * @var \B2T\Pdfgenerator\Model\PdfgeneratorFactory
     */
    private $pdfgeneratorFactory;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * PdfgeneratorRepository constructor.
     * @param TemplateResource $resource
     * @param TemplatesInterface $templatesInterface
     * @param PdfgeneratorFactory $pdfgeneratorFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        TemplateResource $resource,
        TemplatesInterface $templatesInterface,
        PdfgeneratorFactory $pdfgeneratorFactory,
        ManagerInterface $messageManager
    ) {
        $this->resource = $resource;
        $this->templatesInterface = $templatesInterface;
        $this->pdfgeneratorFactory = $pdfgeneratorFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @param TemplatesInterface|Pdfgenerator $template
     * @return TemplatesInterface
     */
    public function save(TemplatesInterface $template)
    {
        try {
            $this->resource->save($template);
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, 'There was a error');
        }

        return $template;
    }

    /**
     * @param int $templateId
     * @return mixed
     */
    public function getById($templateId)
    {
        if (!isset($this->instances[$templateId])) {
            $template = $this->pdfgeneratorFactory->create();
            $this->resource->load($template, $templateId);

            $this->instances[$templateId] = $template;
        }

        return $this->instances[$templateId];
    }

    /**
     * @param TemplatesInterface|Pdfgenerator $template
     * @return bool
     */
    public function delete(TemplatesInterface $template)
    {
        $id = $template->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($template);
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, 'There was a error');
        }

        unset($this->instances[$id]);

        return true;
    }

    /**
     * @param int $templateId
     * @return bool
     */
    public function deleteById($templateId)
    {
        $template = $this->getById($templateId);
        return $this->delete($template);
    }
}
